import Hello from "./components/Hello"
import './App.css';
import Change from "./components/Change";
import Connection from "./Connection";


function App() {
  return (
    <div className="App">
      <Hello name="Emmanuel" last_name="Ouzan" />
      <Hello name="Yossef" last_name="Gabay" />
      <Change />
      <Connection />
    </div>
  );
}

export default App;
