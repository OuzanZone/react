import InputBox from "./components/InputBox";
import Header from "./components/Header";
import { useState } from 'react'

function Connection() {
    const [text, setText] = useState('Empty')
    return <div>
        <Header value={text} />
        <InputBox change={setText} />
    </div>
}

export default Connection