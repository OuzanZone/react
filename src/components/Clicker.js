import { useState } from 'react'

function Clicker() {

    const [counter, setCounter] = useState(0)
    function clicked() {
        setCounter(counter +1)
    }
    return <div>
        <h1>Clicked {counter} Times</h1>
        <button onClick={clicked}>Click Me!!</button>
    </div>
}

export default Clicker