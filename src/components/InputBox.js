function InputBox(props) {
    return <input onChange={e => {
        props.change(e.target.value)
    }}
    ></input>
}

export default InputBox