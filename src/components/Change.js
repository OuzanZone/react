import { useState } from 'react'  
function Change() {
    const [text, setText] = useState('Empty')

    function update(e) {
        setText(e.target.value)
    }
    return <div>
        <h3>{text}</h3>
        <input onChange={update}></input>
    </div>
}

export default Change